/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package web;

import data.ClienteDaoJDBC;
import dominio.Cliente;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author David
 */
@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {

    private HttpSession session;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        ResourceBundle resource;
        Locale locale;
        
        String mylanguage = request.getParameter("lang");

        if (mylanguage != null) {
            String[] choosenLanguage = mylanguage.split("_");
            String language = choosenLanguage[0];
            String country = choosenLanguage[1];
            locale = new Locale(language, country);
            session.setAttribute("language", locale);
            resource = ResourceBundle.getBundle("i18n.Bundle", locale);
        } else {
            locale = (Locale) session.getAttribute("language");
            if(locale != null){
                resource = ResourceBundle.getBundle("i18n.Bundle", locale);
            } else {
                resource = ResourceBundle.getBundle("i18n.Bundle", request.getLocale());
            }
            
        }

        //session.setAttribute("language", request.getLocale());
        session.setAttribute("colName", resource.getString("name"));
        session.setAttribute("colSurname", resource.getString("surname"));
        session.setAttribute("colEmail", resource.getString("email"));
        session.setAttribute("colPhone", resource.getString("phone"));
        session.setAttribute("colBalance", resource.getString("balance"));
        session.setAttribute("addClientButton", resource.getString("add-client"));
        session.setAttribute("pageTitle", resource.getString("page-title"));
        session.setAttribute("pageSubtitle", resource.getString("page-subtitle"));
        session.setAttribute("adminMenu", resource.getString("admin-menu"));

        this.accionDefault(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion != null) {
            switch (accion) {
                case "insertar":
                    this.insertarCliente(request, response);
                    break;

                case "eliminar":
                    break;

                default:
                    this.accionDefault(request, response);
            }
        } else {
            this.accionDefault(request, response);
        }
    }

    private void accionDefault(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Cliente> clientes = new ClienteDaoJDBC().listar();
        //System.out.println("clientes = " + clientes);
        session = request.getSession();
        session.setAttribute("clientes", clientes);

        //request.getRequestDispatcher("clientes.jsp").forward(request, response);
        response.sendRedirect("clientes.jsp");
    }

    private void insertarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String balanceString = request.getParameter("balance");

        double balance = 0;
        if (balanceString != null && !"".equals(balanceString)) {
            balance = Double.parseDouble(balanceString);
        }

        //crear el cliente
        Cliente cliente = new Cliente(name, surname, email, phone, balance);
        int rowsAfected = new ClienteDaoJDBC().insertar(cliente);

        //se cuentan las filas afectadas que debe ser 1 y se envia la variable de confirmacion al jsp
        if (rowsAfected > 0) {
            session.setAttribute("resultado", "El usuario: " + name + " " + surname + " se ha insertado correctamente.");
        }

        this.accionDefault(request, response);
    }
}
