<%-- 
    Document   : clientes
    Created on : 28/11/2021, 11:22:01 PM
    Author     : David
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate value="${date}" pattern="yyyy" var="currentYear" />
<fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm" var="now" />
<fmt:setLocale value="es_MX"/>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${pageSubtitle} | <%=request.getLocale()%> </title>
        <link rel="icon" href="${pageContext.request.contextPath}/resources/images/brand/favicon.png"/> 
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" />
        <link href="${pageContext.request.contextPath}/resources/css/my-styles.css" rel="stylesheet" >
        <link href="${pageContext.request.contextPath}/resources/css/loading.css" rel="stylesheet" >
    </head>
    <body onload="access()">
        <!-- Loading -->
        <%@include file="includes/loading.jspf" %>
        <!--Header and NavBar-->
        <%@include file="includes/header.jspf" %>
        <!--Toast-->
        <%@include file="includes/toast.jspf" %>

        <h1 class="text-center py-3">${pageSubtitle}  ${language}</h1>

        <div class="main mx-auto col-lg-10">
            <div class="card">
                <div class="card-header bg-primary text-light">
                    ${pageTitle}
                </div>
                <div class="p-3">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                        ${addClientButton}
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="text-light">
                                <tr class="thead-light">
                                    <th scope="col">ID</th>
                                    <th scope="col">${colName}</th>
                                    <th scope="col">${colSurname}</th>
                                    <th scope="col">${colEmail}</th>
                                    <th scope="col">${colPhone}</th>
                                    <th scope="col">${colBalance}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="cliente" items="${clientes}">
                                    <tr id="${cliente.idCliente}">
                                        <th scope="row">${cliente.idCliente}</th>
                                        <td>${cliente.nombre}</td>
                                        <td>${cliente.apellido}</td>
                                        <td>${cliente.email}</td>
                                        <td>${cliente.telefono}</td>
                                        <td><fmt:formatNumber currencySymbol="$" type="currency" value="${cliente.saldo}"/>  </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal -->
        <%@include file="includes/modal.jspf" %>

        <!--Footer-->
        <%@include file="includes/footer.jspf" %>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"></script>
        <script>
        $(function () {
            setTimeout(function () {
//                    $("#loading").addClass("hidden");
                $("#loading").addClass("hidden");
            }, 1000);
        });
        function access() {
            var result = "${sessionScope.resultado}";
            if (result !== "") {
                $(".message-body.toast-body").text(result);
                $(".message-title").text("Eureka!");
                $('.toast').toast("show");
            }
        }
        </script>
        <script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
    </body>
</html>
